import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useSelector } from "react-redux";
import CustomerTableHeader from "./CustomerTableHeader";
import CustomerData from "./CustomerData";
import AddCustomerButton from "./AddCustomerButton";
import CreateCustomerForm from "./CreateCustomerForm"

const CustomersListing = () => {
	const customers = useSelector((state) => state.customers.entities);
	const error = useSelector((state) => state.customers.error);
	const loading = useSelector((state) => state.customers.loading);
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true)

	const [searchTerm, setSearchTerm] = useState("");
	const handleChange = (e) => {
		setSearchTerm(e.target.value);
	};
	function search(customers) {
		return customers.filter(
			(customer) =>
				customer.customerId
					.toString()
					.toLowerCase()
					.indexOf(searchTerm.toLowerCase()) > -1 ||
				customer.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
				customer.age.toString().indexOf(searchTerm.toLowerCase()) > -1 ||
				customer.email.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
				customer.address
					.toString()
					.toLowerCase()
					.indexOf(searchTerm.toLowerCase()) > -1 ||
				customer.phoneNumber
					.toString()
					.toLowerCase()
					.indexOf(searchTerm.toLowerCase()) > -1
		);
	}

	if (error) {
		return <div className="d-flex justify-content-center">{error.message}</div>;
	}

	if (loading) {
		return (
			<div className="d-flex justify-content-center">
				<div
					className="spinner-border m-5"
					style={{ width: "4rem", height: "4rem" }}
					role="status"
				>
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
	}

	return (
		<div className="container">
			<div className="form-group col-md-5">
			<button className="btn btn-secondary" onClick={() => handleShow()}>
				Add Customer
			</button>
			<Modal show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title>Fill Details </Modal.Title>
				</Modal.Header>
				<Modal.Body>
				<CreateCustomerForm handleClose={handleClose}/>
				</Modal.Body>
				<Modal.Footer>
					<button variant="secondary" onClick={handleClose}>
						Close
					</button>
				</Modal.Footer>
			</Modal>
			</div>
			<div className="form-group col-md-5">
				<input
					type="text"
					placeholder="Search"
					value={searchTerm}
					onChange={handleChange}
				/>
			</div>
			<table className="table table-striped">
				<CustomerTableHeader></CustomerTableHeader>
				<CustomerData searchResults={search(customers)} flag="true" />
			</table>
		</div>
	);
};

export default CustomersListing;
