import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { updateCustomer } from "../actions";

const UpdateCustomerForm = ({ fetchCustomer, setFetchCustomer }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [customerId, setCustomerId] = useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const custId = window.location.pathname.split("/")[2];

  const customers = useSelector((state) => state.customers.entities);

  const selectedCustomer = customers.filter((m) => m.customerId == custId)[0];

  const [customer, setCustomer] = useState(selectedCustomer);

  const handleSubmit = (e) => {
    e.preventDefault();
    // dispatch updateCustomer action
    dispatch(
      updateCustomer({
        customerId: customer.customerId,
        name: customer.name,
        age: customer.age,
        email: customer.email,
        address: customer.address,
        phoneNumber: customer.phoneNumber,
      })
    )
      .then(() => {
        console.log("updateCustomer is successful");
        setFetchCustomer(!fetchCustomer);
      })
      .catch(() => {})
      .finally(() => {
        console.log("updateCustomer thunk function is completed");
        history.push("/find");
      });
  };

  return (
    <div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
      <h3>Update Customer</h3>
      <form onSubmit={handleSubmit} autoComplete="off">
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="name">Name:</label>
            <input
              id="name"
              type="text"
              className="form-control"
              value={customer.name}
              onChange={(e) =>
                setCustomer({ ...customer, name: e.target.value })
              }
            />
          </div>

          <div className="form-group col-md-5">
            <label htmlFor="age">Age:</label>
            <input
              id="type"
              type="number"
              className="form-control"
              value={customer.age}
              onChange={(e) =>
                setCustomer({ ...customer, age: e.target.value })
              }
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="email">Email:</label>
            <input
              id="email"
              type="text"
              className="form-control"
              value={customer.email}
              onChange={(e) =>
                setCustomer({ ...customer, email: e.target.value })
              }
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="address">Address:</label>
            <input
              id="address"
              type="text"
              className="form-control"
              value={customer.address}
              onChange={(e) =>
                setCustomer({ ...customer, address: e.target.value })
              }
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="phoneNumber">Phone Number:</label>
            <input
              id="phoneNumber"
              type="text"
              className="form-control"
              value={customer.phoneNumber}
              onChange={(e) =>
                setCustomer({ ...customer, phoneNumber: e.target.value })
              }
            />
          </div>
        </div>
        <div className="form-group">
          <input type="submit" value="Update Customer" />
        </div>
      </form>
    </div>
  );
};

export default UpdateCustomerForm;
