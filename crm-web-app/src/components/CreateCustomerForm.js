import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addCustomer } from "../actions";
import { Formik, Form, Field } from "formik";
import { fetchCustomers } from "../actions";

const CreateCustomerForm = ({ fetchCustomer, setFetchCustomer, handleClose}) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const [customerId, setCustomerId] = useState("");
	const [name, setName] = useState("");
	const [age, setAge] = useState("");
	const [email, setEmail] = useState("");
	const [address, setAddress] = useState("");
	const [phoneNumber, setPhoneNumber] = useState("");
	const [focused, setFocused] = useState(false);
	
	return (
		<div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
			
			<Formik
				initialValues={{
					customerId: "",
					name: "",
					age: "",
					email: "",
					address: "",
					phoneNumber: "",
				}}
				validate={(values) => {
					const errors = {};
					if (!values.name) {
						errors.name = "Name is required";
					}

					if (!values.age) {
						errors.age = "Age is required";
					} else if (values.age > 100) {
						errors.age = "Age is Invalid";
					}

					if (!values.email) {
						errors.email = "Email is required";
					} else if (
						!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
					) {
						errors.email = "Invalid email address";
					}

					if (!values.address) {
						errors.address = "Address is required";
					}
					if (!values.phoneNumber) {
						errors.phoneNumber = "Phone is required";
					}else if(!/^[+]?(?:[0-9]{2})?[0-9]{10}$/i.test(values.phoneNumber)
						
					){
						errors.phoneNumber = "Invalid phone number";
					}

					return errors;
				}}
				onSubmit={(values, { setSubmitting }) => {
					dispatch(
						addCustomer({
							customerId: customerId,
							name: values.name,
							age: values.age,
							email: values.email,
							address: values.address,
							phoneNumber: values.phoneNumber,
						})
					)
						.then(() => {
							console.log("addCustomer is successful");
							setFetchCustomer(!fetchCustomer);
						})
						.catch(() => {})
						.finally(() => {
							console.log("addCustomer thunk function is completed");
							setSubmitting(false);
							history.push("/find");
							dispatch(fetchCustomers());
							handleClose();
						});
				}}
			>
				{({
					values,
					errors,
					touched,
					handleChange,
					handleBlur,
					handleSubmit,
					isSubmitting,
				}) => (
					<form onSubmit={handleSubmit} autoComplete="off">
						
						<div className="form-row">
							<div className="form-group col-md-5">
								<label htmlFor="name" style={{color: 'purple'}}>Name:*</label>
								<input
									id="name"
									type="text"
									className="form-control"
									placeholder="Enter your full Name"
									value={values.name}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								{errors.name && touched.name && errors.name}
							</div>

							<div className="form-group col-md-5">
								<label htmlFor="age"style={{color: 'purple'}}>Age:*</label>
								<input
									id="age"
									type="number"
									className="form-control"
									placeholder="Enter your Age"
									value={values.age}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								{errors.age && touched.age && errors.age}
							</div>
							<div className="form-group col-md-5">
								<label htmlFor="email"style={{color: 'purple'}}>Email:*</label>
								<input
									id="email"
									type="text"
									className="form-control"
									placeholder="abc@email.com"
									value={values.email}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								{errors.email && touched.email && errors.email}
							</div>
							<div className="form-group col-md-5">
								<label htmlFor="address"style={{color: 'purple'}}>Address:*</label>
								<input
									id="address"
									type="text"
									className="form-control"
									placeholder="Enter your Address"
									value={values.address}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								{errors.address && touched.address && errors.address}
							</div>
							<div className="form-group col-md-5">
								<label htmlFor="phoneNumber"style={{color: 'purple'}}>Contact Number:*</label>
								<input
									id="phoneNumber"
									type="text"
									className="form-control"
									placeholder="Enter Phone Number"
									value={values.phoneNumber}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								{errors.phoneNumber &&
									touched.phoneNumber &&
									errors.phoneNumber}
							</div>
						</div>
						
						<div className="form-group">
							<input
								type="submit"
								value="Add Customer"
								disabled={isSubmitting}
							/>
						</div>
						
					</form>
				)}
			</Formik>
			
			
		</div>
	);
};

export default CreateCustomerForm;
