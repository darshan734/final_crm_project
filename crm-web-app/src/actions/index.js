import axios from "axios";

// for our Action Creator
export const fetchCustomersBegin = () => {
  return {
    type: "FETCH_CUSTOMERS_BEGIN",
  };
};

export const fetchCustomersSuccess = (customers) => {
  return {
    type: "FETCH_CUSTOMERS_SUCCESS",
    payload: customers,
  };
};

export const fetchCustomersFailure = (err) => {
  return {
    type: "FETCH_CUSTOMERS_FAILURE",
    payload: { message: "Failed to fetch customers.. please try again later" },
  };
};

export const getApiStatusBegin = () => {
  return {
    type: "GET_APISTAUS_BEGIN",
  };
};

export const getApiStatusSuccess = (status) => {
  return {
    type: "GET_APISTATUS_SUCCESS",
    payload: status,
  };
};

export const getApiStatusFailure = (err) => {
  return {
    type: "GET_APISTATUS_FAILURE",
    payload: { message: "Failed to get staus.. please try again later" },
  };
};

// to be call by the components
export const fetchCustomers = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(fetchCustomersBegin());
    //console.log("state after fetchCustomersBegin", getState());
    axios.get("http://localhost:8080/crm/customer/findall").then(
      (res) => {
        setTimeout(() => {
          console.log("response in action fetchCustomer:", res.data);
          dispatch(fetchCustomersSuccess(res.data));

          //console.log("state after fetchCustomersSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch FETCH_CUSTOMERS_FAILURE
        dispatch(fetchCustomersFailure(err));
        //console.log("state after fetchCustomersFailure", getState());
      }
    );
  };
};

export const addCustomerBegin = () => {
  return {
    type: "ADD_CUSTOMER_BEGIN",
  };
};

export const addCustomerSuccess = () => {
  return {
    type: "ADD_CUSTOMER_SUCCESS",
  };
};

export const addCustomerFailure = (err) => {
  return {
    type: "ADD_CUSTOMER_FAILURE",
    payload: { message: "Failed to add new customer.. please try again later" },
  };
};

export const updateCustomerBegin = () => {
  return {
    type: "UPDATE_CUSTOMER_BEGIN",
  };
};

export const updateCustomerSuccess = () => {
  return {
    type: "UPDATE_CUSTOMER_SUCCESS",
  };
};

export const updateCustomerFailure = (err) => {
  return {
    type: "UPDATE_CUSTOMER_FAILURE",
    payload: { message: "Failed to update customer.. please try again later" },
  };
};

export const deleteCustomerBegin = () => {
  return {
    type: "DELETE_CUSTOMER_BEGIN",
  };
};

export const deleteCustomerSuccess = () => {
  return {
    type: "DELETE_CUSTOMER_SUCCESS",
  };
};

export const deleteCustomerFailure = (err) => {
  return {
    type: "DELETE_CUSTOMER_FAILURE",
    payload: { message: "Failed to delete customer.. please try again later" },
  };
};

function delay(t, v) {
  return new Promise(function (resolve) {
    setTimeout(resolve.bind(null, v), t);
  });
}

export const addCustomer = (customer) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    return axios.post("http://localhost:8080/crm/customer/add", customer).then(
      () => {
        //console.log("customer created!");
        // this is where we can dispatch ADD_CUSTOMER_SUCCESS
        dispatch(addCustomerSuccess());
      },
      (err) => {
        dispatch(addCustomerFailure(err));
        //console.log("state after addCustomerFailure", getState());
      }
    );
  };
};

export const getApiStatus = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(getApiStatusBegin());
    //console.log("state after getApiStatusBegin", getState());
    axios.get("http://localhost:8080/crm/interactions/status").then(
      (res) => {
        setTimeout(() => {
          dispatch(getApiStatusSuccess(res.data));
          //console.log(res.data);
          //	console.log("state after getApiStatusSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch GET_APISTATUS_FAILURE
        dispatch(getApiStatusFailure(err));
        //console.log("state after getApiStatusFailure", getState());
      }
    );
  };
};

export const updateCustomer = (customer) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    dispatch(updateCustomerBegin());
    return axios
      .put("http://localhost:8080/crm/customer/update", customer)
      .then(
        () => {
          //console.log("customer updated!");
          // this is where we can dispatch UPDATE_CUSTOMER_SUCCESS
          dispatch(updateCustomerSuccess());
        },
        (err) => {
          dispatch(updateCustomerFailure(err));
          //	console.log("state after updateCustomerFailure", getState());
        }
      );
  };
};

const findByIdSuccess = (res) => {
  return { type: "FIND_BY_ID_SUCCESS", payload: res };
};
const findByIdFailure = (err) => {
  return { type: "FIND_BY_ID_FAILURE", payload: err };
};
export const findCustomerById = (value) => {
  return (dispatch, getState) => {
    return axios.get(`http://localhost:8080/crm/customer/find/${value}`).then(
      (res) => {
        dispatch(findByIdSuccess(res));
      },
      (err) => {
        dispatch(findByIdFailure(err));
      }
    );
  };
};

const findByNameSuccess = (res) => {
  console.log("3");
  return { type: "FIND_BY_NAME_SUCCESS", payload: res };
};
const findByNameFailure = (err) => {
  return { type: "FIND_BY_NAME_FAILURE", payload: err };
};
export const findCustomerByName = (value) => {
  console.log("2");
  return (dispatch, getState) => {
    return axios
      .get(`http://localhost:8080/crm/customer/findbyname/${value}`)
      .then(
        (res) => {
          dispatch(findByNameSuccess(res));
        },
        (err) => {
          dispatch(findByNameFailure(err));
        }
      );
  };
};
const fetchInteractionsSuccess = (res) => {
  return { type: "FIND_INTERACTIONS_SUCCESS", payload: res };
};
const fetchInteractionsFailure = (err) => {
  return { type: "FIND_INTERACTIONS_FAILURE", payload: err };
};
export const fetchInteractions = (value) => {
  return (dispatch, getState) => {
    return axios
      .get(`http://localhost:8080/crm/interactions/find/${value}`)
      .then(
        (res) => {
          dispatch(fetchInteractionsSuccess(res));
        },
        (err) => {
          dispatch(fetchInteractionsFailure(err));
        }
      );
  };
};
const addInteractionsSuccess = (res) => {
  console.log("response coming", res);
  return { type: "ADD_INTERACTIONS_SUCCESS", payload: res };
};
const addInteractionsFailure = (err) => {
  return { type: "ADD_INTERACTIONS_FAILURE", payload: err };
};

export const addInteractions = (data) => {
  return (dispatch, getState) => {
    return axios.post(`http://localhost:8080/crm/interactions/add`, data).then(
      (res) => {
        dispatch(addInteractionsSuccess(res));
      },
      (err) => {
        dispatch(addInteractionsFailure(err));
      }
    );
  };
};

const updateInteractionsSuccess = (res) => {
  return { type: "UPDATE_INTERACTIONS_SUCCESS", payload: res };
};
const updateInteractionsFailure = (err) => {
  return { type: "UPDATE_INTERACTIONS_FAILURE", payload: err };
};

export const updateInteractions = (data) => {
  return (dispatch, getState) => {
    return axios
      .put(`http://localhost:8080/crm/interactions/update`, data)
      .then(
        (res) => {
          dispatch(updateInteractionsSuccess(res));
        },
        (err) => {
          dispatch(updateInteractionsFailure(err));
        }
      );
  };
};

export const deleteCustomer = (customerId) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    dispatch(deleteCustomerBegin());
    return axios
      .post(`http://localhost:8080/crm/customer/delete/${customerId}`)
      .then(
        () => {
          console.log("customer deleted!");
          // this is where we can dispatch Delete_CUSTOMER_SUCCESS
          dispatch(deleteCustomerSuccess());
        },
        (err) => {
          console.log(customerId);
          dispatch(deleteCustomerFailure(err));
          console.log("state after deleteCustomerFailure", getState());
        }
      );
  };
};
