const initialState = { entities: [] };

export default (state = initialState, action) => {
  console.log(`received ${action.type} dispatch in deleteCustomerReducer`);
  switch (action.type) {
    case "DELETE_CUSTOMER_BEGIN":
      return { ...state, loading: true, error: null };
    case "DELETE_CUSTOMER_SUCCESS":
      return { ...state, loading: false };
    case "DELETE_CUSTOMER_FAILURE":
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
