package com.allstate.crm.service;

import com.allstate.crm.entities.Interaction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ComponentScan("com.allstate.crm")
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class InteractionServiceImplTest {

    @Autowired
    InteractionServiceImpl interactionServiceImpl;

    private Interaction interaction;

    @Test
    public void testAddInteraction() {
        interaction = new Interaction(111, 1, "issues with headset", new Date(),"text");
        assertEquals(1L, interactionServiceImpl.addInteraction(interaction));
    }

    @Test
    public void testFindInteractionByCustomerId(){
        assertNotNull(interactionServiceImpl.findInteraction(1));
    }

    @Test
    public void testUpdateInteraction() {
        interaction = new Interaction(111, 21, "payment failed", new Date(),"call");
        assertEquals(1L, interactionServiceImpl.updateInteraction(interaction));
    }

    @Test
    public void testDeleteInteractionByCustomerId(){
        assertEquals(1L, interactionServiceImpl.deleteInteraction(1));
    }
}
