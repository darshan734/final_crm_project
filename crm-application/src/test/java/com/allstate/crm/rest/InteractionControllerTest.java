package com.allstate.crm.rest;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class InteractionControllerTest {

    @Autowired
    InteractionController interactionController;

    private Interaction interaction;

    @Test
    public void testAddInteractionAndReturnTrue() {
        interaction = new Interaction(121, 3, "Not able to place order", new Date(),"email");
        assertEquals(1L, interactionController.addInteraction(interaction));
    }
    @Test
    public void testFindInteractionByCustomerIdWhenInteractionExist() {
        ResponseEntity<List> response = interactionController.findInteraction(3);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void testFindInteractionByCustomerIdWhenInteractionNotExist() {
        ResponseEntity<List> response = interactionController.findInteraction(1234);
        assertThat(response.getBody()).isNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
    @Test
    public void testUpdateInteractionAndReturnFalseIfInteractionFound() {
        interaction = new Interaction(121, 3, "Payment Failed", new Date(),"text");
        int response=interactionController.updateInteraction(interaction);
        assertNotNull(response);
        assertEquals(1L, response );

    }
    @Test
    public void testUpdateInteractionAndReturnFalseIfInteractionNotFound() {
        interaction = new Interaction(121, 213, "Payment Failed", new Date(),"text");
        int response=interactionController.updateInteraction(interaction);
        assertNotNull(response);
        assertEquals(0L, response );

    }

    @Test
    public void testDeleteInteractionAndReturnTrueIfInteractionFound() {
        int response = interactionController.deleteInteraction(3);
        assertNotNull(response);
        assertEquals(1L, response);
    }

    @Test
    public void testDeleteInteractionAndReturnFalseIfInteractionNotFound() {
        int response = interactionController.deleteInteraction(1231);
        assertNotNull(response);
        assertEquals(0L, response);
    }
}
