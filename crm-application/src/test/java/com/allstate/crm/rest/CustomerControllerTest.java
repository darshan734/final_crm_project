package com.allstate.crm.rest;

import com.allstate.crm.dao.CustomerDao;
import com.allstate.crm.entities.Customer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class CustomerControllerTest {

    @Autowired
    CustomerController customerController;

    private Customer customer;

    @Test
    public void testAddCustomerAndReturnTrue()  {
        customer = new Customer(3, "Henry", 26, "bbb@gmail.com","123 Marine Palace ", "9245412");
        assertEquals(1L, customerController.addCustomer(customer));
    }

    @Test
    public void testFindByCustomerIdWhenValidIdGiven() {
        ResponseEntity<Customer> response = customerController.findCustomerById(3);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getCustomerId()).isEqualTo(3);
        assertThat(response.getBody().getName()).isEqualTo("Henry");
        assertThat(response.getBody().getAge()).isEqualTo(26);
        assertThat(response.getBody().getEmail()).isEqualTo("bbb@gmail.com");
        assertThat(response.getBody().getAddress()).isEqualTo("123 Marine Palace ");
        assertThat(response.getBody().getPhoneNumber()).isEqualTo("9245412");
    }

    @Test
    public void testFindByCustomerIdWhenValidIdNotGiven() {
        ResponseEntity<Customer> response = customerController.findCustomerById(5);
        assertThat(response.getBody()).isNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void testFindCustomerByNameWhenNameIsNotNull() {
        ResponseEntity<List> response = customerController.findCustomerByName("Henry");
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void testFindCustomerByNameWhenNameIsNull() {
        ResponseEntity<List> response = customerController.findCustomerByName(" ");
        assertThat(response.getBody()).isNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void testFindAllCustomerAndReturnTrue() {
        ResponseEntity<List> response = customerController.findAll();
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void testUpdateCustomerAndReturnTrueIfCustomerFound() {
        customer = new Customer(3, "Henry", 29, "avb@gmail.com", "123,South plaza", "123456");
        int response=customerController.updateCustomer(customer);
        assertEquals(1L, response );
        assertNotNull(response);
    }

    @Test
    public void testUpdateCustomerAndReturnFalseIfCustomerNotFound() {
        customer = new Customer(153, "Henry", 29, "avb@gmail.com", "123,South plaza", "123456");
        int response=customerController.updateCustomer(customer);
        assertEquals(0, response );
        assertNotNull(response);
    }

    @Test
    public void testDeleteCustomerAndReturnTrueIfCustomerFound() {
        int response = customerController.deleteCustomer(3);
        assertNotNull(response);
        assertEquals(1L, response);
    }

    @Test
    public void testDeleteCustomerAndReturnFalseIfCustomerNotFound() {
        int response = customerController.deleteCustomer(153);
        assertNotNull(response);
        assertEquals(0L, response);
    }
}