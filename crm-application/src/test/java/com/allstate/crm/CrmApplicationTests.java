package com.allstate.crm;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
class CrmApplicationTests {

	@Test
	void contextLoads() {
	}
	@Autowired
	private TestRestTemplate restTemplate;

	int port = 8080;

	@Test
	public void restApiTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "crm/customer/findall",
				String.class)).isNotNull();
	}
}
