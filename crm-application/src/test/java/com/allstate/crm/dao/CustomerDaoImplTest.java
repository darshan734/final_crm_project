package com.allstate.crm.dao;


import com.allstate.crm.entities.Customer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ComponentScan("com.allstate.crm")
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomerDaoImplTest {

    @Autowired
    CustomerDao customerDao;

    @Autowired
    MongoTemplate tpl;

    private Customer customer;

    @Test
    public void testAddCustomerAndFindCustomerById() {
        Date date=new Date();
        customer = new Customer(2, "Marry", 26, "bbb@gmail.com","123 marine palace ", "9245412");
        customerDao.addCustomer(customer);
        assertEquals(customer.getCustomerId(), customerDao.findCustomerById(2).getCustomerId());
    }

    @Test
    public void testFindCustomerByName(){
        List<Customer> customers=customerDao.findCustomerByName("Marry");
        assertNotNull(customers);
    }

    @Test
    public void testFindAllCustomer(){
        List<Customer> customers=customerDao.findAllCustomer();
        assertNotNull(customers);
    }

    @Test
    public void testDeleteCustomer(){
        assertEquals(1L, customerDao.deleteCustomer(6));
    }

    @Test
    public void testUpdateCustomer() {
        customer = new Customer(2, "Marry", 26, "bbb@gmail.com","123, Marina street", "9244412");
        assertEquals(1L, customerDao.updateCustomer(customer));
    }
}
