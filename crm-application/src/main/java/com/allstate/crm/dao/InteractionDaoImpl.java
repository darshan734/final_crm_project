package com.allstate.crm.dao;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.exceptions.InteractionException;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InteractionDaoImpl implements InteractionDao {

    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public List<Interaction> findInteraction(int customerId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("customerId").is(customerId)).with(Sort.by(Sort.Direction.DESC, "interactionDateAndTime"));
        return mongoTemplate.find(query, Interaction.class);
    }

    @Override
    public int addInteraction(Interaction interaction) throws InteractionException {
        Interaction interactionAdded= mongoTemplate.insert(interaction);
        if (interactionAdded !=null)
        {return 1;}
        else
        {return 0;}
    }

    @Override
    public int updateInteraction(Interaction interaction) throws InteractionException {
        Query query = new Query();
        query.addCriteria(Criteria.where("interactionId").is(interaction.getInteractionId()));
        Update update = new Update();
        update.set("interactionDetails", interaction.getInteractionDetails());
       /* update.set("age", customer.getAge());
        update.set("address", customer.getAddress());
        update.set("email", customer.getEmail());
        update.set("phoneNumber", customer.getPhoneNumber());*/

        UpdateResult updateResult=mongoTemplate.updateFirst(query, update, Interaction.class);
        if(updateResult!=null){
            return (int) updateResult.getModifiedCount();
        }else {
            return 0;
        }
    }

    @Override
    public int deleteInteration(int customerId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("customerId").is(customerId));
        DeleteResult deleteResult= mongoTemplate.remove(query, Interaction.class);
        if(deleteResult!=null) {
            return (int) deleteResult.getDeletedCount();
        }else{
            return 0;
        }

    }
}
