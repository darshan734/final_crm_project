package com.allstate.crm.service;

import com.allstate.crm.dao.InteractionDao;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.exceptions.InteractionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
@Service
public class InteractionServiceImpl implements InteractionService {

    @Autowired
    private InteractionDao dao;
    @Autowired
    NextSequenceService nextSequenceService;

    @Override
    public List<Interaction> findInteraction(int customerId) {
        List<Interaction> interactions=null;
        if(customerId >0) {
            interactions= dao.findInteraction(customerId);
            if(interactions==null|| interactions.size()==0){
                throw new InteractionException("No Interaction details found for Customer Id:"+customerId);
            }
        }
        else {
            throw new IllegalArgumentException("The CustomerId parameter cannot be null.");
        }
        return interactions;
    }



    @Override
    public int addInteraction(Interaction interaction) throws InteractionException {
        int rowsAdded=0;

        if (validate(interaction)) {
            interaction.setInteractionId(nextSequenceService.getNextInteractionSequence("interactionSequences"));
            interaction.setInteractionDateAndTime(new Date());
            rowsAdded= dao.addInteraction(interaction);
            if(rowsAdded==0){
                throw new InteractionException("Issue with adding interaction data for interaction:"+interaction.toString());
            }

        } else {
            throw new IllegalArgumentException("Invalid Data");
        }
        return rowsAdded;
    }

    @Override
    public int deleteInteraction(int  customerId) throws InteractionException {
        int rowsDeleted=0;
        if(customerId >0) {
            rowsDeleted= dao.deleteInteration(customerId);
            if(rowsDeleted==0){
                throw new InteractionException("Issue with deleting interaction data for customerId: "+customerId);
            }
        }
        else {
            throw new IllegalArgumentException("The CustomerId parameter cannot be null.");
        }
        return rowsDeleted;
    }

    @Override
    public int updateInteraction(Interaction interaction) throws InteractionException {
        int rowsUpdated=0;
        if (validateForUpdate(interaction)) {
            rowsUpdated= dao.updateInteraction(interaction);
            if(rowsUpdated==0){
                throw new InteractionException("Update not happened for interaction : "+interaction.toString());
            }
        } else {
            throw new InteractionException("Invalid Data");
        }
        return rowsUpdated;
    }

    public static boolean validate(Interaction interaction) {
        return  interaction!=null&&
                interaction.getCustomerId() >0
                && interaction.getInteractionType() != null
                && interaction.getInteractionDetails() != null;
    }

    public static boolean validateForUpdate(Interaction interaction) {
        return interaction.getInteractionId() > 0
                && interaction.getInteractionDetails() != null;

    }

}
