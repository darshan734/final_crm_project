package com.allstate.crm.service;

import com.allstate.crm.dao.CustomerDao;
import com.allstate.crm.dao.InteractionDao;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.exceptions.CustomerException;
import com.allstate.crm.exceptions.InteractionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private CustomerDao dao;
    @Autowired
    NextSequenceService nextSequenceService;
    @Override
    public List<Customer> findCustomerByName(String name) {
        List<Customer> customers=null;
        if(name !=null && !name.isBlank()) {
            customers=dao.findCustomerByName(name);
            if(customers==null|| customers.size()==0){
                throw new CustomerException("No customer details found for Name:"+name);
            }
        }
        else {
            throw new IllegalArgumentException("The Customer name parameter cannot be null or blank.");
        }
        return customers;
    }

    @Override
    public Customer findCustomerById(int customerId) {
        Customer customer=null;
        if(customerId >0) {
            customer=dao.findCustomerById(customerId);
            if(customer==null){
                throw new CustomerException("No customer details found for Customer Id:"+customerId);
            }
        }
        else {
            throw new IllegalArgumentException("The CustomerId parameter cannot be null.");
        }
        return customer;
    }

    @Override
    public List<Customer> findAllCustomer() {
        List<Customer> customers=null;
        customers=dao.findAllCustomer();
        if(customers==null|| customers.size()==0){
            throw new CustomerException("No customer details found");
        }
        return customers;
    }

    @Override
    public int addCustomer(Customer customer) throws CustomerException {
        int rowsAdded=0;
        if (validate(customer)) {
            customer.setCustomerId(nextSequenceService.getNextSequence("customSequences"));
            rowsAdded=dao.addCustomer(customer);
             if(rowsAdded==0){
                 throw new CustomerException("Issue with adding customer data for customer"+customer.toString());
             }
        } else {
            throw new IllegalArgumentException("Invalid Data.");
        }
        return rowsAdded;
    }

    @Override
    public int deleteCustomer(int customerId) {
        int rowsDeleted=0;
        if(customerId >0) {
            rowsDeleted= dao.deleteCustomer(customerId);
            if(rowsDeleted==0){
                throw new CustomerException("Issue with deleting customer data for customerId: "+customerId);
            }
        }
        else {
            throw new IllegalArgumentException("The CustomerId parameter cannot be null.");
        }
        return rowsDeleted;
    }

    @Override
    public int updateCustomer(Customer customer) throws CustomerException {
        int rowsUpdated=0;
        if (validate(customer)) {
            rowsUpdated= dao.updateCustomer(customer);
            if(rowsUpdated==0){
                throw new CustomerException("Update not happened customer for customer: "+customer.toString());
            }

        } else {
            throw new IllegalArgumentException("Invalid Data");
        }
        return rowsUpdated;
    }

    public static boolean validate(Customer customer) {
        return customer!=null
                && customer.getName()!=null
                && customer.getAge()>0
                && customer.getAddress()!= null
                && customer.getEmail()!=null
                && customer.getPhoneNumber()!=null;

    }
}
