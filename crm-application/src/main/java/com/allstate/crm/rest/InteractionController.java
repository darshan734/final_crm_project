package com.allstate.crm.rest;


import com.allstate.crm.entities.Interaction;
import com.allstate.crm.exceptions.InteractionException;
import com.allstate.crm.service.InteractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("crm/interactions")
public class InteractionController {

    @Autowired
    InteractionService interactionService;
    Logger logger = Logger.getLogger(InteractionController.class.getName());

    @GetMapping(value = "/status")
    public String getStatus()
    {
        logger.info("Interactions status method");
        return "Interactions Rest Api is up and running";
    }
    @RequestMapping(value = "/add" ,method = RequestMethod.POST)
    public int addInteraction(@RequestBody Interaction interaction) {
        return interactionService.addInteraction(interaction);
    }
    @RequestMapping(value = "/update" ,method = RequestMethod.PUT)
    public int updateInteraction(@RequestBody Interaction interaction) {
        return interactionService.updateInteraction(interaction);
    }
    @RequestMapping(value = "/delete/{customerId}" ,method = RequestMethod.POST)
    public int deleteInteraction(@RequestBody@PathVariable("customerId") int customerId) {
        return interactionService.deleteInteraction(customerId);
    }
    @RequestMapping(value = "/find/{customerId}", method = RequestMethod.GET)
    public ResponseEntity<List> findInteraction(@PathVariable("customerId") int customerId) {
        List<Interaction>interactionList= interactionService.findInteraction(customerId);
        return new ResponseEntity<List>(interactionList, HttpStatus.OK);
    }
    @ExceptionHandler({InteractionException.class})
    public ResponseEntity<String> handleException(InteractionException e) {
        logger.warning(String.format("interaction Exception raised: %s", e.getMessage()));
        return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
    }



}
