package com.allstate.crm.exceptions;

public class InteractionException extends RuntimeException{
    public InteractionException(String message){
        super(message);
    }
}
